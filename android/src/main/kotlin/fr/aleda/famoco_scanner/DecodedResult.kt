package fr.aleda.famoco_scanner

class DecodedResult(
    val symbology: Int,
    val length: Int,
    val data: ByteArray?,
) {
    fun toMap(): HashMap<String, Any?> {
        val map = HashMap<String, Any?>()

        map[DecodedResult::symbology.name] = symbology
        map[DecodedResult::length.name] = length
        map[DecodedResult::data.name] = data
        map[DecodedResult::barcode.name] = barcode

        return map
    }

    val barcode: String?
        get() = if (data == null) null else String(data, 0, length)
}