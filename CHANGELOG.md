# CHANGELOG

## 1.0.1

* Removed unused package dependency

## 1.0.0

* Only the Famoco 325 & 335 implementation.
* Enable & disable the scan
* Edit on focus
* Continous / Non stop scan
