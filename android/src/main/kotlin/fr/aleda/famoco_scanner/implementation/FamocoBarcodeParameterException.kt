package fr.aleda.famoco_scanner.implementation

class FamocoBarcodeParameterException(var paramId: Int, var paramValue: Int, var result: Int) :
    Exception() {
    override val message: String
        get() = "Parameter $paramId is invalid. Value=$paramValue, Result=$result"
}