package fr.aleda.famoco_scanner

class PhysicalButtonResult(
    val isKeyDown: Boolean
) {
    fun toMap(): Map<String, Any> {
        val data = HashMap<String, Any>()
        data["isKeyDown"] = isKeyDown

        return data
    }
}