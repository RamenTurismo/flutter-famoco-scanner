package fr.aleda.famoco_scanner.implementation

import android.app.Instrumentation
import android.content.Context
import android.os.Build
import android.view.KeyCharacterMap
import android.view.KeyEvent
import fr.aleda.famoco_scanner.DecodedResult
import fr.aleda.famoco_scanner.ScannedEventChannel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import java.util.concurrent.Executors
import kotlin.coroutines.CoroutineContext

abstract class FamocoScannerBase {
    protected val coroutineScope = CustomCoroutineScope()

    companion object {
        /**
         * Gets the instance based on the device.
         */
        fun getInstance(
            context: Context,
            scannedEventChannel: ScannedEventChannel
        ): FamocoScannerBase {
            if (FamocoSupportedDevices.isFX3X5()) {
                return Famoco325(
                    context,
                    scannedEventChannel::notify,
                    scannedEventChannel::notifyFail
                )
            }

            throw UnsupportedOperationException("The device ${Build.MODEL} isn't supported.")
        }

        /**
         * Send scanned digit as keyboard entry if option is enabled
         */
        fun sendKeyEvents(scanResult: String) {
            val chars = scanResult.toCharArray()
            val charMap: KeyCharacterMap = KeyCharacterMap.load(KeyCharacterMap.VIRTUAL_KEYBOARD)

            val events: Array<KeyEvent> = charMap.getEvents(chars)

            val inst = Instrumentation()
            events.filterIndexed { index, _ -> index % 2 == 0 }.forEach {
                inst.sendKeyDownUpSync(it.keyCode)
            }
        }
    }

    abstract val options: FamocoOptions;
    abstract fun enableScanner()
    abstract fun disableScanner()
    abstract fun stopScan()
    abstract fun startScan(handsFreeScan: Boolean): Boolean
    abstract fun initialize()
}

class CustomCoroutineScope internal constructor() : CoroutineScope {
    private val dispatcher = Executors.newSingleThreadExecutor()
        .asCoroutineDispatcher()

    override val coroutineContext: CoroutineContext =
        dispatcher + Job() + CoroutineExceptionHandler { coroutineContext: CoroutineContext, throwable: Throwable ->
            GlobalScope.launch { println("Caught $throwable") }
        }
}