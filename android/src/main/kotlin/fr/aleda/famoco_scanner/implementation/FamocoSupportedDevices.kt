package fr.aleda.famoco_scanner.implementation

import android.os.Build

class FamocoSupportedDevices {
    companion object {
        private const val FX325_BUILD_MODEL = "FX325,1"
        private const val FX335_BUILD_MODEL = "FX335,1"

        fun isFX3X5(): Boolean {
            return (Build.MODEL == FX325_BUILD_MODEL) || (Build.MODEL == FX335_BUILD_MODEL)
        }
    }
}