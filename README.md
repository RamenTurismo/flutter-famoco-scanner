# README

A flutter implementation of the Famoco scanner device.

## Requirements

- Minimum version is for Android 10 (API 28).

## Compatible devices

Those mentioned in the [SDK](https://help.famoco.com/developers/devices/FX325/FX325/).

Compatible with :

- FX 325 (`FX325,1`)
- FX 335 (`FX335,1`)

Tested on :

- FX 335 (`FX335,1`)

## Usage

1. Make your `MainActivity` inherit `FamocoScanPhysicalButtonHandlerActivity`. If you found a better way to handle the physical button, you can ignore this step.

   Ignoring this step also means `FamocoScanner.instance.scanPhysicalButton` will not be called.

1. Simply use `FamocoScanner.instance`. e.g.

   ```dart
   FamocoScanner.instance.startScan();
   ```

   To intercept a barcode, you'll only need to use `decodedComplete` on a `StatefulWidget`:

   ```dart
    @override
    void initState() {
        super.initState();
        _onDecodedSubscription = FamocoScanner.instance.decodedComplete.listen(
            (data) {
                setState(() {
                    _lastBarcodeScanned = data.barcode;
                });
            });
    }

    @override
    void dispose() {
        super.dispose();
        _onDecodedSubscription.cancel();
    }
   ```

   The method `FamocoScanner.instance.enableScanner();` is automatically called on subscribing to `decodedComplete`, same thing when calling `cancel` on the subscription, `disableScanner` is called.

ℹ️ The instance of the implementation is automatically chosen based on the device's brand.

## Example

See the [example app](example/README.md).

This is a replica of the app found in the SDK documentation : [Demo app (v1.3, source code)](https://help.famoco.com/developers/devices/FX325/imager/fx325-imager/).