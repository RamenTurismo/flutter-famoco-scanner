package fr.aleda.famoco_scanner

import android.view.KeyEvent
import io.flutter.embedding.android.FlutterActivity

/**
 * To handle the physical button of the device, this activity needs to be inherited
 * from the MainActivity of the App.
 *
 * See the example app.
 *
 * Until [Volume keys can't be caught by focusScope with autofocus](https://github.com/flutter/flutter/issues/71144) is fixed.
 */
open class FamocoScanPhysicalButtonHandlerActivity : FlutterActivity() {
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if ((keyCode == KeyEvent.KEYCODE_SHIFT_LEFT || keyCode == KeyEvent.KEYCODE_F1 || keyCode == KeyEvent.KEYCODE_F2)
            && event!!.repeatCount == 0
        ) {
            notify(isKeyDown = true)
        }

        return super.onKeyDown(keyCode, event)
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_SHIFT_LEFT || keyCode == KeyEvent.KEYCODE_F1 || keyCode == KeyEvent.KEYCODE_F2) {
            notify(isKeyDown = false)
        }

        return super.onKeyUp(keyCode, event)
    }

    private fun notify(isKeyDown: Boolean) {
        FamocoScannerPlugin.scanPhysicalButtonEventChannel.notify(PhysicalButtonResult(isKeyDown))
    }
}