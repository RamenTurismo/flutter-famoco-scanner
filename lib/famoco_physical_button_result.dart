class FamocoPhysicalButtonResult {
  final bool isKeyDown;

  FamocoPhysicalButtonResult({
    required this.isKeyDown,
  });

  factory FamocoPhysicalButtonResult.fromMap(Map<String, dynamic> map) {
    return FamocoPhysicalButtonResult(
      isKeyDown: map['isKeyDown'] as bool,
    );
  }
}
