import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'famoco_decoded_result.dart';
import 'famoco_physical_button_result.dart';

class FamocoScanner {
  static FamocoScanner? _instance;
  static FamocoScanner get instance => _instance ??= FamocoScanner._();

  final _scannedEventChannel =
      const EventChannel('fr.aleda/famoco_scanner/scanned');

  /// Happens when the device scanner has finished decoding the barcode.
  Stream<FamocoDecodedResult> get decodedComplete => _scannedEventChannel
      .receiveBroadcastStream()
      .map((event) => FamocoDecodedResult.fromMap(
          Map.castFrom(event as Map<dynamic, dynamic>)));

  final _scanPhysicalButtonEventChannel =
      const EventChannel('fr.aleda/famoco_scanner/scan_physical_button');

  /// Notifies when the user presses the physical scan button,
  /// on the left of the device.
  ///
  /// This Stream is only fed when `FamocoScanPhysicalButtonHandlerActivity` has been inherited from in
  /// `MainActivity`, located in `android` → `app` → `src` → `main` → `kotlin`.
  /// example :
  ///
  /// ```kotlin
  /// package fr.aleda.famoco_scanner_example
  ///
  /// import fr.aleda.famoco_scanner.FamocoScanPhysicalButtonHandlerActivity
  ///
  /// class MainActivity: FamocoScanPhysicalButtonHandlerActivity()
  /// ```
  Stream<FamocoPhysicalButtonResult> get scanPhysicalButton =>
      _scanPhysicalButtonEventChannel.receiveBroadcastStream().map((event) =>
          FamocoPhysicalButtonResult.fromMap(
              Map.castFrom(event as Map<dynamic, dynamic>)));

  @visibleForTesting
  final methodChannel = const MethodChannel('fr.aleda/famoco_scanner');

  FamocoScanner._();

  /// Enables the scanner.
  ///
  /// Automatically called when [decodedComplete] is listened to.
  Future<void> enableScanner() async {
    await methodChannel.invokeMethod("enableScanner");
  }

  /// Disables the scanner and clean the resources.
  ///
  /// Automatically called when [decodedComplete] is canceled.
  Future<void> disableScanner() async {
    await methodChannel.invokeMethod("disableScanner");
  }

  /// Enables the device scanner, activating the red laser.
  ///
  /// [isHandsFree] allows for a continous scan.
  ///
  /// Returns if the start was successful via a [bool].
  Future<bool> startScan({bool isHandsFree = false}) async {
    bool? result = await methodChannel.invokeMethod<bool>(
      "startScan",
      <String, dynamic>{
        "isHandsFree": isHandsFree,
      },
    );

    return result ?? false;
  }

  /// Stops the device scanner, disables the red laser.
  Future<void> stopScan() async {
    await methodChannel.invokeMethod('stopScan');
  }

  /// Will override the options that are non null.
  ///
  /// [onDecodedUseKeyboardEvents] allows to simulate keyboard events when the user scans.
  Future<void> setOptions({
    Map<int, int>? barcodeParams,
    int? surfaceTextureTexName,
    bool? onDecodedUseKeyboardEvents,
  }) async {
    Map<String, dynamic> arguments = {};
    arguments.putIfNotNull('barcodeParams', barcodeParams);
    arguments.putIfNotNull('surfaceTextureTexName', surfaceTextureTexName);
    arguments.putIfNotNull(
        'onDecodedUseKeyboardEvents', onDecodedUseKeyboardEvents);

    if (arguments.isEmpty) return;

    await methodChannel.invokeMethod('setOptions', arguments);
  }
}

extension MapExtensions<K, V> on Map<K, V> {
  void putIfNotNull(K key, V? value) {
    if (value == null) return;

    putIfAbsent(key, () => value);
  }
}
