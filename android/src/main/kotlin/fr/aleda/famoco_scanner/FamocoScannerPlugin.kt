package fr.aleda.famoco_scanner

import android.util.Log
import androidx.annotation.NonNull
import fr.aleda.famoco_scanner.implementation.FamocoScannerBase
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

class FamocoScannerPlugin : FlutterPlugin, MethodChannel.MethodCallHandler {
    companion object {
        val scanPhysicalButtonEventChannel = ScanPhysicalButtonEventChannel()
    }

    private lateinit var famocoScannerBase: FamocoScannerBase
    private lateinit var channel: MethodChannel
    private lateinit var scannedEventChannel: ScannedEventChannel

    override fun onAttachedToEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        // onDecoded event.
        scannedEventChannel = ScannedEventChannel(
            onListen = { famocoScannerBase.enableScanner() },
            onCancel = { famocoScannerBase.disableScanner() },
        )
        scannedEventChannel.onAttachedToEngine(binding)

        // Physical button event.
        scanPhysicalButtonEventChannel.onAttachedToEngine(binding)

        // Method calls
        channel = MethodChannel(binding.binaryMessenger, "fr.aleda/famoco_scanner")
        channel.setMethodCallHandler(this)

        // Initialize implementation
        famocoScannerBase =
            FamocoScannerBase.getInstance(binding.applicationContext, scannedEventChannel)
        famocoScannerBase.initialize()
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        scannedEventChannel.onDetachedFromEngine(binding)
        scanPhysicalButtonEventChannel.onDetachedFromEngine(binding)

        channel.setMethodCallHandler(null)
        famocoScannerBase.disableScanner()
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: MethodChannel.Result) {
        when (call.method) {
            ::enableScanner.name -> {
                enableScanner(call, result)
            }

            ::disableScanner.name -> {
                disableScanner(call, result)
            }

            ::startScan.name -> {
                startScan(call, result)
            }

            ::stopScan.name -> {
                stopScan(call, result)
            }

            ::setOptions.name -> {
                setOptions(call, result)
            }

            else -> {
                result.notImplemented()
            }
        }
    }

    private fun enableScanner(@NonNull call: MethodCall, @NonNull result: MethodChannel.Result) {
        try {
            famocoScannerBase.enableScanner()
        } catch (e: java.lang.Exception) {
            result.error("Generic", e.message, e.stackTrace.map { it.toString() })
            return
        }

        result.success(null)
    }

    private fun disableScanner(@NonNull call: MethodCall, @NonNull result: MethodChannel.Result) {
        famocoScannerBase.disableScanner()

        result.success(null)
    }

    private fun startScan(@NonNull call: MethodCall, @NonNull result: MethodChannel.Result) {
        val isHandsFree = call.argumentNotNull<Boolean>("isHandsFree")

        val isSuccessful = famocoScannerBase.startScan(isHandsFree)

        result.success(isSuccessful)
    }

    private fun stopScan(@NonNull call: MethodCall, @NonNull result: MethodChannel.Result) {
        famocoScannerBase.stopScan()

        result.success(null)
    }

    private fun setOptions(@NonNull call: MethodCall, @NonNull result: MethodChannel.Result) {
        call.argument<Map<Int, Int>>("barcodeParams")
            ?.apply {
                famocoScannerBase.options.barcodeParams.putAll(this)
            }
        call.argument<Int>("surfaceTextureTexName")
            ?.apply {
                famocoScannerBase.options.surfaceTextureTexName = this
            }
        call.argument<Boolean>("onDecodedUseKeyboardEvents")
            ?.apply {
                famocoScannerBase.options.onDecodedUseKeyboardEvents = this
            }

        result.success(null)
    }
}

fun <T> MethodCall.argumentNotNull(key: String): T {
    return this.argument<T>(key)
        ?: throw UnsupportedOperationException("Argument not found: ${key}.")
}
