package fr.aleda.famoco_scanner.implementation

class FamocoOptions(
    val barcodeParams: HashMap<Int, Int>,
    var surfaceTextureTexName: Int,
    var onDecodedUseKeyboardEvents: Boolean,
) {
    companion object {
        val default: FamocoOptions = FamocoOptions(
            barcodeParams = hashMapOf(
                Pair(687, 4),
                Pair(764, 0),
                Pair(765, 0),
                Pair(8610, 1),
                Pair(8611, 0)
            ),
            surfaceTextureTexName = 5,
            onDecodedUseKeyboardEvents = false,
        );
    }
}