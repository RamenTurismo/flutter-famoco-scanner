package fr.aleda.famoco_scanner

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.EventChannel

class ScannedEventChannel(val onListen: () -> Unit, val onCancel: () -> Unit) : FlutterPlugin,
    EventChannel.StreamHandler {
    private lateinit var eventChannel: EventChannel
    private var sink: EventChannel.EventSink? = null

    override fun onAttachedToEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        eventChannel =
            EventChannel(flutterPluginBinding.binaryMessenger, "fr.aleda/famoco_scanner/scanned")
        eventChannel.setStreamHandler(this)
    }

    override fun onDetachedFromEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        eventChannel.setStreamHandler(null)
    }

    override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
        sink = events
        onListen()
    }

    override fun onCancel(arguments: Any?) {
        sink = null
        onCancel()
    }

    fun notify(onDecodeResult: DecodedResult) {
        sink?.success(onDecodeResult.toMap())
    }

    fun notifyFail(errorCode: Int) {
        sink?.error(errorCode.toString(), "BarcodeReader failed with $errorCode", null)
    }
}