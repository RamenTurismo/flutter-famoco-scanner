import 'dart:typed_data';

class FamocoDecodedResult {
  bool get isEmpty => length == 0;

  final int length;

  /// Could be `null` when [length] is 0.
  final String? barcode;

  /// Could be `null` when [length] is 0.
  final Uint32List? data;
  final int symbology;

  FamocoDecodedResult({
    required this.length,
    this.barcode,
    this.data,
    required this.symbology,
  });

  factory FamocoDecodedResult.fromMap(Map<String, dynamic> map) {
    return FamocoDecodedResult(
      length: map['length'] as int,
      barcode: map['barcode'] != null ? map['barcode'] as String : null,
      data: map['data'] != null
          ? Uint32List.fromList(map['data'] as List<int>)
          : null,
      symbology: map['symbology'] as int,
    );
  }
}
