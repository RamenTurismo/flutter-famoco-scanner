package fr.aleda.famoco_scanner.implementation

import android.content.Context
import android.graphics.SurfaceTexture
import android.graphics.SurfaceTexture.OnFrameAvailableListener
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraManager
import android.provider.Settings
import android.util.Log
import androidx.core.content.ContextCompat.getSystemService
import com.famoco.barcode.BarcodeManager
import com.zebra.adc.decoder.BarCodeReader
import com.zebra.adc.decoder.BarCodeReader.DecodeCallback
import fr.aleda.famoco_scanner.DecodedResult
import kotlinx.coroutines.launch

class Famoco325(
    private val context: Context,
    private val onDecodedCallback: (DecodedResult) -> Unit,
    private val onBarcodeReaderErrorCallback: (errorCode: Int) -> Unit,
) :
    FamocoScannerBase(),
    DecodeCallback,
    BarCodeReader.ErrorCallback,
    OnFrameAvailableListener {

    companion object {
        private val TAG = Famoco325::class.simpleName
    }

    override val options = FamocoOptions.default

    private val barcodeManager: BarcodeManager = BarcodeManager.getInstance(context)
    private var barcodeReader: BarCodeReader? = null
    private var surfaceTexture: SurfaceTexture? = null

    override fun initialize() {
        System.loadLibrary("IAL.zebra")
        System.loadLibrary("SDL.zebra")
        System.loadLibrary("barcodereader90.zebra")
    }

    override fun enableScanner() {
        if (isBarcodeServiceEnabled() && barcodeManager.isActive) {
            barcodeManager.pause() // to avoid any conflict
        }

        val readerId: Int = getScannerCameraId()
        val barCodeReader = BarCodeReader.open(readerId, context)

        // The new SDK requires set preview texture
        val surfaceTexture = SurfaceTexture(options.surfaceTextureTexName)

        initializeBarcodeReader(barCodeReader, surfaceTexture)

        this.surfaceTexture = surfaceTexture
        this.barcodeReader = barCodeReader
    }

    override fun disableScanner() {
        barcodeReader?.release()
        barcodeReader = null

        if (isBarcodeServiceEnabled() && !barcodeManager.isActive) {
            // make life easier for other apps
            barcodeManager.resume()
        }
    }

    override fun startScan(handsFreeScan: Boolean): Boolean {
        val res = if (handsFreeScan) {
            barcodeReader?.startHandsFreeDecode(BarCodeReader.ParamVal.HANDSFREE.toInt())
        } else {
            barcodeReader?.startDecode()
        }

        return res == BarCodeReader.BCR_SUCCESS
    }

    override fun stopScan() {
        barcodeReader?.stopDecode()
    }

    override fun onDecodeComplete(
        symbology: Int,
        length: Int,
        data: ByteArray?,
        reader: BarCodeReader?
    ) {
        if (length <= 0 || data == null || data.isEmpty()) {
            return
        }

        val result = DecodedResult(symbology, length, data)

        if (options.onDecodedUseKeyboardEvents && result.barcode != null) {
            coroutineScope.launch {
                sendKeyEvents(result.barcode!!)
            }
        }

        onDecodedCallback(result)
    }

    override fun onEvent(symbology: Int, length: Int, data: ByteArray?, reader: BarCodeReader?) {
        // nothing to do
    }

    override fun onError(errorCode: Int, p1: BarCodeReader?) {
        onBarcodeReaderErrorCallback(errorCode)
    }

    override fun onFrameAvailable(surfaceTexture: SurfaceTexture?) {
        // nothing to do
    }

    private fun isBarcodeServiceEnabled(): Boolean {
        return try {
            Settings.Global.getInt(
                context.contentResolver,
                "barcode_service_enabled"
            ) == 1
        } catch (e: Settings.SettingNotFoundException) {
            Log.e(TAG, e.message, e)

            false
        }
    }

    @Throws(CameraAccessException::class)
    private fun getScannerCameraId(): Int {
        return getNumberOfCameras() - 1
    }

    @Throws(CameraAccessException::class)
    private fun getNumberOfCameras(): Int {
        val manager = getSystemService(context, CameraManager::class.java) as CameraManager
        return manager.cameraIdList.size
    }

    private fun setBarcodeParameters(
        barCodeReader: BarCodeReader,
        params: Map<Int, Int>
    ) {
        for (param in params) {
            setBarcodeParameter(barCodeReader, param.key, param.value)
        }
    }

    private fun setBarcodeParameter(
        barCodeReader: BarCodeReader,
        paramId: Int,
        paramValue: Int
    ) {
        val result = barCodeReader.setParameter(paramId, paramValue)

        if (result != BarCodeReader.BCR_SUCCESS) {
            throw FamocoBarcodeParameterException(
                paramId,
                paramValue,
                result
            )
        }
    }

    private fun initializeBarcodeReader(
        barCodeReader: BarCodeReader,
        surfaceTexture: SurfaceTexture
    ) {
        barCodeReader.setDecodeCallback(this)
        barCodeReader.setErrorCallback(this)
        setBarcodeParameters(barCodeReader, options.barcodeParams)

        surfaceTexture.setOnFrameAvailableListener(this)
        barCodeReader.setPreviewTexture(surfaceTexture)
    }
}